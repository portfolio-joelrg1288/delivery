import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

import 'package:delivery/theme.dart';

// Pages
import 'package:delivery/src/pages/home/home_page.dart';
import 'package:delivery/src/pages/details/details_page.dart';

// Providers
import 'package:delivery/src/providers/products_provider.dart';
import 'package:delivery/src/providers/categories_provider.dart';

// Services
import 'package:delivery/src/data/services/remote_config_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await initHiveForFlutter();
  await RemoteConfigService.instance.initRemoteConfig();
  await DotEnv.load();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => CategoriesProvider()),
      ChangeNotifierProvider(create: (_) => ProductsProvider()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    final HttpLink httpLink = HttpLink(DotEnv.env['SERVER_API']!);
    final AuthLink authLink =
        AuthLink(getToken: () => 'Bearer ${DotEnv.env['TOKEN']}');
    final Link link = authLink.concat(httpLink);
    final ValueNotifier<GraphQLClient> graphQLClient = ValueNotifier(
      GraphQLClient(
        link: link,
        cache: GraphQLCache(store: HiveStore()),
      ),
    );
    return ScreenUtilInit(
      designSize: Size(360, 690),
      builder: () => GraphQLProvider(
        client: graphQLClient,
        child: MaterialApp(
          title: 'Joelivery',
          theme: DeliveryTheme.theme,
          debugShowCheckedModeBanner: false,
          home: HomePage(),
          routes: <String, WidgetBuilder>{
            HomePage.id: (_) => HomePage(),
            DetailsPage.id: (_) => DetailsPage(),
          },
        ),
      ),
    );
  }
}
