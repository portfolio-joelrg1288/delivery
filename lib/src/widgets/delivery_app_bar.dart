import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:delivery/theme.dart';

class DeliveryAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget> actions;

  const DeliveryAppBar({Key? key, required this.title, this.actions = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 5.h,
      actions: actions,
      centerTitle: true,
      backgroundColor: Colors.white,
      shadowColor: grey.withOpacity(0.25),
      iconTheme: IconThemeData(color: darkBlue),
      title: Text(title,
          textAlign: TextAlign.left,
          style: Theme.of(context).textTheme.headline3),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(60.h);
}
