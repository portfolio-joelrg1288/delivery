import 'package:another_flushbar/flushbar.dart';

import 'package:flutter/material.dart';

class DeliveryFlushbar {
  static showFlushbar(BuildContext context, String title, String? desc,
      {bool success = false}) {
    Flushbar(
      title: title,
      message: desc,
      borderRadius: BorderRadius.circular(8),
      duration: Duration(seconds: 3),
      backgroundColor: success ? Colors.green : Colors.red,
      margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
    )..show(context);
  }
}
