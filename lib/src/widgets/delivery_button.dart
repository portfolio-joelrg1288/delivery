import 'package:delivery/src/providers/products_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

import 'package:delivery/theme.dart';

class DeliveryButton extends StatelessWidget {
  final String text;
  final double? height;
  final Color textColor;
  final TextStyle? textStyle;
  final double? borderRadius;
  final Color backgroundColor;
  final void Function() callback;
  const DeliveryButton({
    Key? key,
    required this.text,
    this.height,
    this.textStyle,
    this.borderRadius,
    this.textColor = Colors.white,
    this.backgroundColor = aquamarine,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        height: height ?? 45.h,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: buttonGradient),
          borderRadius: BorderRadius.circular(borderRadius ?? 10.h),
        ),
        child: Center(
          child: Text(
            text,
            style: textStyle ??
                Theme.of(context)
                    .textTheme
                    .headline2!
                    .copyWith(color: textColor),
          ),
        ),
      ),
    );
  }
}
