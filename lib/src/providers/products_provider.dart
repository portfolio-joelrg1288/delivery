import 'package:delivery/src/models/customer_model.dart';
import 'package:flutter/material.dart' show ChangeNotifier;

import 'package:delivery/src/models/product_model.dart';
import 'package:delivery/src/data/services/products_service.dart';

class ProductsProvider with ChangeNotifier {
  List<ProductModel>? _popular;
  ProductModel? _selectedProduct;
  List<ProductModel>? _recommendations;
  CustomerModel? _customer;

  List<ProductModel>? get popular => this._popular;

  ProductModel? get selectedProduct => this._selectedProduct;

  List<ProductModel>? get recommendations => this._recommendations;

  CustomerModel? get customer => this._customer;

  set selectedProduct(ProductModel? v) => this._selectedProduct = v;

  set customer(CustomerModel? v) => this._customer = v;

  void onLoadProducts() async {
    _popular = await ProductsService().retrieveProducts();
    _recommendations = await ProductsService().retrieveProducts(popular: false);
    notifyListeners();
  }

  void onFavourite({required int index, bool popular = true}) {
    if (popular)
      _popular![index].liked = !_popular![index].liked;
    else
      _recommendations![index].liked = !_recommendations![index].liked;
    notifyListeners();
  }
}
