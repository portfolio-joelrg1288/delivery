import 'dart:convert';

import 'package:flutter/material.dart' show ChangeNotifier;

import 'package:delivery/src/utils/hex_color.dart';
import 'package:delivery/src/models/category_model.dart';
import 'package:delivery/src/data/services/remote_config_service.dart';

class CategoriesProvider with ChangeNotifier {
  List<CategoryModel>? _categories;

  List<CategoryModel>? get categories => this._categories;

  void onLoadCategories() async {
    final _aux = RemoteConfigService.instance.getString('categories');
    final parsedJson = Map<String, dynamic>.from(json.decode(_aux));
    parsedJson.removeWhere((key, value) => !value['active']);
    parsedJson.forEach((key, value) {
      final section = CategoryModel(
        id: key,
        image: value['image'],
        title: value['title'],
        color: HexColor(value['color'] ?? '#FFFFFF'),
      );
      this._categories = [...(this._categories ?? []), section];
    });
    this._categories = this._categories ?? [];
    notifyListeners();
  }
}
