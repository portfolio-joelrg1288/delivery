import 'package:cloud_firestore/cloud_firestore.dart';

import 'firebase_service.dart';
import 'package:delivery/src/models/product_model.dart';

class ProductsService {
  static String collection = 'products';
  final _firebaseService = FirebaseService(collection);

  Future<String> createProduct(ProductModel product) async {
    try {
      return (await _firebaseService.post(product.toMap())).id;
    } catch (e) {
      print(e);
      throw (e);
    }
  }

  Future<List<ProductModel>> retrieveProducts({bool popular = true}) async {
    final collection = _firebaseService.getCollection();
    final Query query = collection.where('popular', isEqualTo: popular);
    final QuerySnapshot products = await query.get();
    return products.docs
        .map((element) =>
            ProductModel.fromMap(element.data() as Map<String, dynamic>))
        .toList();
  }
}
