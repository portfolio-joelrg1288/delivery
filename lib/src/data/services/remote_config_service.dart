import 'package:firebase_remote_config/firebase_remote_config.dart';

class RemoteConfigService {
  final _remoteConfig = RemoteConfig.instance;

  RemoteConfigService.constructor();

  static final RemoteConfigService instance = RemoteConfigService.constructor();

  Future<bool> initRemoteConfig() async {
    await _remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: Duration(seconds: 0),
      minimumFetchInterval: Duration(seconds: 0),
    ));
    return await _remoteConfig.fetchAndActivate();
  }

  String getString(String key) => _remoteConfig.getString(key);
}
