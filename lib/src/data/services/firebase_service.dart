import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseService {
  final String collection;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  FirebaseService(this.collection);

  CollectionReference getCollection() => _firestore.collection(collection);

  Future<DocumentReference> post(Map<String, dynamic> body) async =>
      await _firestore.collection(collection).add(body);

  Future<void> update(String id, Map<String, dynamic> body) async =>
      await _firestore
          .collection(collection)
          .doc(id)
          .set(body, SetOptions(merge: true));

  Future<List<QueryDocumentSnapshot>> list() async =>
      (await _firestore.collection(collection).get()).docs;

  Future<Stream<dynamic>> listenAllSnapshots() async =>
      _firestore.collection(collection).snapshots();

  Future<Stream<dynamic>> listenSnapshotsById(String id) async =>
      _firestore.collection(collection).doc(id).snapshots();
}
