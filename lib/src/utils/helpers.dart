import 'package:intl/intl.dart';

class Helpers {
  static String priceFormat(double price) {
    return NumberFormat("#,##0.00", "en_US").format(price);
  }
}
