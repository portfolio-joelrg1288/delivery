import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;

import 'package:delivery/theme.dart';
import 'package:delivery/src/utils/helpers.dart';
import 'package:delivery/src/widgets/delivery_button.dart';
import 'package:delivery/src/widgets/delivery_flushbar.dart';
import 'package:delivery/src/providers/products_provider.dart';

class DetailsPage extends StatelessWidget {
  static const String id = "/details";
  final String purchaseQuery = """
  mutation Purchase(\$offerId: ID!) {
    purchase(offerId: \$offerId) {
      success,
      errorMessage,
      customer {
        id,
        name,
        balance,
        offers {
          id,
          price,
          price
        }
      }
    }
  }
  """;

  @override
  Widget build(BuildContext context) {
    final productsProviderReader = context.read<ProductsProvider>();
    final String productPrice =
        Helpers.priceFormat(productsProviderReader.selectedProduct!.price!);
    final double _priceSize = _setPriceFont(productPrice);

    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.55,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: CachedNetworkImageProvider(
                      productsProviderReader.selectedProduct!.backgroundImage!),
                  fit: BoxFit.fill,
                ),
              ),
              child: Align(
                alignment: Alignment.topLeft,
                child: SafeArea(
                  child: Padding(
                    padding:
                        EdgeInsets.only(left: 30.w, top: 10.h, right: 30.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child:
                              Icon(Icons.arrow_back_ios, color: Colors.white),
                        ),
                        Text(productsProviderReader.selectedProduct!.title!,
                            style: Theme.of(context)
                                .textTheme
                                .headline1!
                                .copyWith(color: Colors.white, fontSize: 13.w)),
                        SizedBox(width: 24.w),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.45,
                padding: EdgeInsets.only(top: 20.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20.w),
                      child: Text('Description',
                          style: Theme.of(context).textTheme.headline1),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20.w, top: 5.h),
                      child: Text(
                        productsProviderReader.selectedProduct!.description!,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                    if (productsProviderReader.selectedProduct!.ingredients !=
                        null)
                      Padding(
                        padding: EdgeInsets.only(
                            left: 20.w, top: 10.h, bottom: 10.h),
                        child: Text('Ingredients',
                            style: Theme.of(context).textTheme.headline1),
                      ),
                    if (productsProviderReader.selectedProduct!.ingredients !=
                        null)
                      Container(
                        height: MediaQuery.of(context).size.height * 0.125,
                        child: ListView.separated(
                            padding: EdgeInsets.only(left: 20.w),
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) => Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width: 70.w,
                                      height: 80.w,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.w),
                                        image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: CachedNetworkImageProvider(
                                              productsProviderReader
                                                  .selectedProduct!
                                                  .ingredients![index]
                                                  .image!),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      productsProviderReader.selectedProduct!
                                          .ingredients![index].title!,
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(fontSize: 9.w, color: grey),
                                    ),
                                  ],
                                ),
                            separatorBuilder: (context, index) =>
                                SizedBox(width: 15.w),
                            itemCount: productsProviderReader
                                .selectedProduct!.ingredients!.length),
                      ),
                    SizedBox(height: 5.h),
                    Divider(color: lightGrey),
                    SizedBox(height: 5.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SafeArea(
                          top: false,
                          left: false,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.55,
                            child: productsProviderReader
                                        .selectedProduct!.ingredients ==
                                    null
                                ? Mutation(
                                    options: MutationOptions(
                                      document: gql(purchaseQuery),
                                      update: (
                                        GraphQLDataProxy cache,
                                        QueryResult? result,
                                      ) {
                                        return cache;
                                      },
                                      onCompleted: (dynamic resultData) {
                                        if (resultData['purchase']['success']) {
                                          Navigator.pop(context);
                                          DeliveryFlushbar.showFlushbar(context,
                                              'Success', 'Product purchased!',
                                              success: true);
                                        } else
                                          DeliveryFlushbar.showFlushbar(
                                              context,
                                              'Error',
                                              resultData!['purchase']![
                                                      'errorMessage'] ??
                                                  'Unknown error');
                                      },
                                    ),
                                    builder: (RunMutation runMutation,
                                        QueryResult? result) {
                                      if (result!.hasException) {
                                        print(result.exception);
                                        return Text(
                                            result.exception.toString());
                                      }
                                      return DeliveryButton(
                                          text: 'Order now',
                                          callback: () => runMutation({
                                                'offerId':
                                                    productsProviderReader
                                                        .selectedProduct?.id,
                                              }));
                                    })
                                : DeliveryButton(
                                    text: 'Order now', callback: () {}),
                          ),
                        ),
                        SafeArea(
                          top: false,
                          right: false,
                          child: Text('\$$productPrice',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1!
                                  .copyWith(fontSize: _priceSize)),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  double _setPriceFont(String price) {
    if (price.length <= 8) return 24.w;
    final lenOverflow = price.length - 8;
    return 24.w - lenOverflow;
  }
}
