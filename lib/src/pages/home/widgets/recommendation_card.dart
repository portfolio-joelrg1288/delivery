import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;

import 'package:delivery/theme.dart';
import 'package:delivery/src/models/product_model.dart';

class RecommendationCard extends StatelessWidget {
  final ProductModel product;
  final Function(ProductModel v) onFavourite;
  final Function(ProductModel v) onSelectProduct;

  const RecommendationCard({
    Key? key,
    required this.product,
    required this.onFavourite,
    required this.onSelectProduct,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onSelectProduct(product),
      child: Container(
        width: 220.w,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              height: 20.h,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.w),
                boxShadow: [
                  BoxShadow(
                    color: lightGrey.withOpacity(0.15),
                    blurRadius: 5,
                    spreadRadius: 4,
                  ),
                ],
              ),
              margin: EdgeInsets.only(
                top: 5.w,
                left: 5.w,
                right: 5.w,
                bottom: 30.h,
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 5.w),
                child: CachedNetworkImage(
                  imageUrl: product.image!,
                  height: double.infinity,
                  width: 80.w,
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                width: 120.w,
                padding: EdgeInsets.only(right: 10.w, top: 10.w),
                margin: EdgeInsets.only(
                  top: 5.w,
                  left: 5.w,
                  right: 5.w,
                  bottom: 25.h,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Organic',
                          style: Theme.of(context)
                              .textTheme
                              .caption!
                              .copyWith(color: lightGrey, fontSize: 9.5.w),
                        ),
                        GestureDetector(
                          onTap: () => onFavourite(product),
                          child: Image.asset(
                              'assets/icons/favourite${product.liked ? '_selected' : ''}.png',
                              width: 15.w),
                        ),
                      ],
                    ),
                    SizedBox(height: 5.h),
                    Text(
                      product.title!,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: 9.5.w, fontWeight: FontWeight.w500),
                    ),
                    Text(
                      product.caption!,
                      style: Theme.of(context)
                          .textTheme
                          .caption!
                          .copyWith(fontSize: 6.5.w),
                    ),
                    SizedBox(height: 5.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '\$${product.price!.toStringAsFixed(2)}',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(fontWeight: FontWeight.w600),
                        ),
                        Container(
                          padding: EdgeInsets.all(2.w),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: grey.withOpacity(0.2),
                                blurRadius: 5,
                                spreadRadius: 1,
                              )
                            ],
                          ),
                          child: Icon(
                            Icons.arrow_forward_ios_rounded,
                            size: 10.w,
                            color: darkBlue,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
