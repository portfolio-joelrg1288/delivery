import 'package:delivery/src/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;

import 'package:delivery/theme.dart';
import 'package:delivery/src/models/product_model.dart';

class ProductCard extends StatelessWidget {
  final ProductModel product;
  final Function(ProductModel v) onFavourite;
  final Function(ProductModel v) onSelectProduct;

  const ProductCard({
    Key? key,
    required this.product,
    required this.onFavourite,
    required this.onSelectProduct,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String productPrice = Helpers.priceFormat(product.price!);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h),
      child: GestureDetector(
        onTap: () => onSelectProduct(product),
        child: Container(
          width: 150.w,
          padding: EdgeInsets.all(10.w),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.w),
              boxShadow: [
                BoxShadow(
                  color: lightGrey.withOpacity(0.15),
                  blurRadius: 5,
                  spreadRadius: 4,
                ),
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  onTap: () => onFavourite(product),
                  child: Image.asset(
                      'assets/icons/favourite${product.liked ? '_selected' : ''}.png',
                      width: 15.w),
                ),
              ),
              Container(
                width: 60.w,
                height: 60.w,
                padding: EdgeInsets.all(10.w),
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: CachedNetworkImageProvider(product.image!),
                      fit: BoxFit.fill),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFFBEE6F5),
                      spreadRadius: 8,
                      blurRadius: 10,
                    )
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      product.title!,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(
                      product.caption!,
                      style: Theme.of(context)
                          .textTheme
                          .caption!
                          .copyWith(fontSize: 7.w),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '\$$productPrice',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(fontWeight: FontWeight.w600),
                        ),
                        Container(
                          padding: EdgeInsets.all(5.w),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: grey.withOpacity(0.2),
                                blurRadius: 5,
                                spreadRadius: 1,
                              )
                            ],
                          ),
                          child: Icon(
                            Icons.arrow_forward_ios_rounded,
                            size: 14.w,
                            color: darkBlue,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
