import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;

import 'package:delivery/theme.dart';
import 'package:delivery/src/models/category_model.dart';

class CategoryCard extends StatelessWidget {
  final CategoryModel category;

  const CategoryCard({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: 60.w,
          height: 60.w,
          decoration: BoxDecoration(
            color: category.color,
            borderRadius: BorderRadius.circular(10.w),
          ),
          child: Center(
            child: Image.asset(
              'assets/images/${category.image}.png',
              width: 50.w,
            ),
          ),
        ),
        Text(
          category.title!,
          style: Theme.of(context)
              .textTheme
              .caption!
              .copyWith(color: darkBlue, fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}
