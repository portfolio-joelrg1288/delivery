import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;

import 'package:delivery/src/models/product_model.dart';
import 'package:delivery/src/models/customer_model.dart';
import 'package:delivery/src/models/category_model.dart';
import 'package:delivery/src/widgets/delivery_app_bar.dart';
import 'package:delivery/src/pages/details/details_page.dart';
import 'package:delivery/src/providers/products_provider.dart';
import 'package:delivery/src/providers/categories_provider.dart';
import 'package:delivery/src/pages/home/widgets/product_card.dart';
import 'package:delivery/src/pages/home/widgets/category_card.dart';
import 'package:delivery/src/pages/home/widgets/recommendation_card.dart';

class HomePage extends StatefulWidget {
  static const String id = "/home";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    Future.microtask(context.read<CategoriesProvider>().onLoadCategories);
    Future.microtask(context.read<ProductsProvider>().onLoadProducts);
  }

  @override
  Widget build(BuildContext context) {
    final String testingQuery = """
    query QueryRoot {
      viewer {
        id,
        name,
        balance,
        offers {
          id,
          price,
          product {
            id,
            name,
            description,
            image
          }
        }
      }
    }
    """;
    final categoriesProviderWatcher = context.watch<CategoriesProvider>();
    final productsProviderWatcher = context.watch<ProductsProvider>();
    final productsProviderReader = context.read<ProductsProvider>();
    List<ProductModel> _offerList = [];
    return Scaffold(
      appBar: DeliveryAppBar(
        title: 'Home',
        actions: [
          Image.asset('assets/icons/notifications.png', width: 20.w),
          SizedBox(width: 15.w),
          Image.asset('assets/icons/complaint.png', width: 20.w),
          SizedBox(width: 20.w),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          ListView(
            padding: EdgeInsets.only(top: 10.h),
            physics: BouncingScrollPhysics(),
            children: [
              if (categoriesProviderWatcher.categories != null &&
                  categoriesProviderWatcher.categories!.isNotEmpty)
                ..._categories(categoriesProviderWatcher.categories!),
              Query(
                options: QueryOptions(
                  document: gql(testingQuery),
                  pollInterval: Duration(seconds: 10),
                ),
                builder: (
                  QueryResult result, {
                  Refetch? refetch,
                  FetchMore? fetchMore,
                }) {
                  if (result.hasException) {
                    print(result.exception);
                    return Container();
                  }
                  if (result.isLoading) {
                    return Center(child: CircularProgressIndicator());
                  }
                  _offerList.clear();
                  productsProviderReader.customer = CustomerModel(
                      id: result.data!['viewer']['id'],
                      name: result.data!['viewer']['name'],
                      balance: result.data!['viewer']['balance'].toDouble());
                  result.data!['viewer']['offers'].forEach((e) {
                    final productCaption =
                        '${e['product']['description'].substring(0, 30)}...';
                    _offerList.add(ProductModel(
                        backgroundImage: e['product']['image'],
                        liked: false,
                        image: e['product']['image'],
                        id: e['id'],
                        title: e['product']['name'],
                        price: e['price'].toDouble(),
                        description: e['product']['description'],
                        caption: productCaption));
                  });
                  return _offers(_offerList, productsProviderReader);
                },
              ),
              if (productsProviderWatcher.popular != null &&
                  productsProviderWatcher.popular!.isNotEmpty)
                ..._popular(productsProviderWatcher, productsProviderReader),
              if (productsProviderWatcher.recommendations != null &&
                  productsProviderWatcher.recommendations!.isNotEmpty)
                ..._recommendations(
                    productsProviderWatcher, productsProviderReader),
            ],
          ),
        ],
      ),
    );
  }

  List<Widget> _categories(List<CategoryModel> categories) {
    return [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Explore categories',
                style: Theme.of(context).textTheme.headline1),
            Text('See all',
                style: Theme.of(context)
                    .textTheme
                    .caption!
                    .copyWith(color: Color(0xFFCFCFCF))),
          ],
        ),
      ),
      SizedBox(
        height: 70.h,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(left: 15.w),
          itemBuilder: (context, index) =>
              CategoryCard(category: categories[index]),
          separatorBuilder: (context, _) => SizedBox(width: 20.w),
          itemCount: categories.length,
        ),
      ),
    ];
  }

  Widget _offers(
      List<ProductModel> offersList, ProductsProvider productsProviderReader) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.w),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10.h),
          child: Text('Ofertas!', style: Theme.of(context).textTheme.headline1),
        ),
        SizedBox(
          height: 200.h,
          child: ListView.separated(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: offersList.length,
            separatorBuilder: (context, _) => SizedBox(width: 10.w),
            itemBuilder: (context, index) => ProductCard(
                product: offersList[index],
                onFavourite: (v) {},
                onSelectProduct: (v) {
                  productsProviderReader.selectedProduct = v;
                  Navigator.pushNamed(context, DetailsPage.id);
                }),
          ),
        ),
      ]),
    );
  }

  List<Widget> _popular(ProductsProvider productsProviderWatcher,
      ProductsProvider productsProviderReader) {
    final List<ProductModel> products = productsProviderWatcher.popular!;
    return [
      Padding(
        padding: EdgeInsets.only(left: 15.w, top: 15.h, bottom: 5.h),
        child: Text('Popular!', style: Theme.of(context).textTheme.headline1),
      ),
      SizedBox(
        height: 200.h,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(left: 15.w),
          itemBuilder: (context, index) => ProductCard(
              product: products[index],
              onFavourite: (v) =>
                  productsProviderReader.onFavourite(index: index),
              onSelectProduct: (v) {
                productsProviderReader.selectedProduct = v;
                Navigator.pushNamed(context, DetailsPage.id);
              }),
          separatorBuilder: (context, _) => SizedBox(width: 10.w),
          itemCount: products.length,
        ),
      ),
    ];
  }

  List<Widget> _recommendations(ProductsProvider productsProviderWatcher,
      ProductsProvider productsProviderReader) {
    final List<ProductModel> products =
        productsProviderWatcher.recommendations!;
    return [
      Padding(
        padding: EdgeInsets.only(left: 15.w, top: 15.h, bottom: 5.h),
        child: Text('Our recommendations',
            style: Theme.of(context).textTheme.headline1),
      ),
      Container(
        height: 110.h,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(left: 15.w),
          itemBuilder: (context, index) => RecommendationCard(
              product: products[index],
              onFavourite: (v) => productsProviderReader.onFavourite(
                  index: index, popular: false),
              onSelectProduct: (v) {
                productsProviderReader.selectedProduct = v;
                Navigator.pushNamed(context, DetailsPage.id);
              }),
          separatorBuilder: (context, _) => SizedBox(width: 10.w),
          itemCount: products.length,
        ),
      ),
    ];
  }
}
