import 'dart:convert';

class CustomerModel {
  final String? id;
  final String? name;
  final double? balance;

  CustomerModel({
    this.id,
    this.name,
    this.balance,
  });

  CustomerModel copyWith({
    String? id,
    String? name,
    double? balance,
  }) {
    return CustomerModel(
      id: id ?? this.id,
      name: name ?? this.name,
      balance: balance ?? this.balance,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'balance': balance,
    };
  }

  factory CustomerModel.fromMap(Map<String, dynamic> map) {
    return CustomerModel(
        id: map['id'], name: map['name'], balance: map['balance']);
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'CustomerModel(id: $id, name: $name, balance: $balance';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CustomerModel &&
        o.id == id &&
        o.name == name &&
        o.balance == balance;
  }

  @override
  int get hashCode {
    return id.hashCode ^ name.hashCode ^ balance.hashCode;
  }
}
