import 'dart:convert';

import 'package:flutter/material.dart' show Color, Colors;

class CategoryModel {
  final String? id;
  final String? title;
  final String? image;
  final Color? color;

  CategoryModel({
    this.id,
    this.title,
    this.image,
    this.color,
  });

  CategoryModel copyWith({
    String? id,
    String? title,
    String? image,
    Color? color,
  }) {
    return CategoryModel(
      id: id ?? this.id,
      title: title ?? this.title,
      image: image ?? this.image,
      color: color ?? this.color,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'image': image,
    };
  }

  factory CategoryModel.fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      id: map['id'] ?? '',
      title: map['title'] ?? '',
      image: map['image'] ?? '',
      color: map['color'] ?? Colors.white,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryModel.fromJson(String source) =>
      CategoryModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CategoryModel(id: $id, title: $title, image: $image, color: $color)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is CategoryModel &&
        o.id == id &&
        o.title == title &&
        o.image == image &&
        o.color == color;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ image.hashCode ^ color.hashCode;
  }
}
