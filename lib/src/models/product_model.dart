import 'dart:convert';

import 'package:delivery/src/models/category_model.dart';

class ProductModel {
  final String? backgroundImage;
  final String? caption;
  final String? category;
  final String? description;
  final String? id;
  final String? image;
  final List<CategoryModel>? ingredients;
  bool liked;
  final bool? popular;
  final double? price;
  final String? title;

  ProductModel({
    this.backgroundImage,
    this.caption,
    this.category,
    this.description,
    this.id,
    this.image,
    this.ingredients,
    required this.liked,
    this.popular,
    this.price,
    this.title,
  });

  ProductModel copyWith({
    String? backgroundImage,
    String? caption,
    String? category,
    String? description,
    String? id,
    String? image,
    List<CategoryModel>? ingredients,
    bool? liked,
    bool? popular,
    double? price,
    String? title,
  }) {
    return ProductModel(
      backgroundImage: backgroundImage ?? this.backgroundImage,
      caption: caption ?? this.caption,
      category: category ?? this.category,
      description: description ?? this.description,
      id: id ?? this.id,
      image: image ?? this.image,
      ingredients: ingredients ?? this.ingredients,
      liked: liked ?? this.liked,
      popular: popular ?? this.popular,
      price: price ?? this.price,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'backgroundImage': backgroundImage,
      'caption': caption,
      'category': category,
      'description': description,
      'id': id,
      'image': image,
      'ingredients': ingredients!.map((e) => e.toMap()).toList(),
      'popular': popular ?? true,
      'price': price,
      'title': title,
    };
  }

  factory ProductModel.fromMap(Map<String, dynamic> map) {
    return ProductModel(
      backgroundImage: map['backgroundImage'],
      caption: map['caption'],
      category: map['category'],
      description: map['description'],
      id: map['id'],
      image: map['image'],
      ingredients: (map['ingredients'] as List<dynamic>)
          .map((e) => CategoryModel.fromMap(e))
          .toList(),
      liked: false,
      popular: map['popular'],
      price: map['price'],
      title: map['title'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductModel.fromJson(String source) =>
      ProductModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProductModel(backgroundImage: $backgroundImage, caption: $caption, category: $category, description: $description, id: $id, image: $image, ingredients: $ingredients, liked: $liked, popular: $popular, price: $price, title: $title)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ProductModel &&
        o.backgroundImage == backgroundImage &&
        o.caption == caption &&
        o.category == category &&
        o.description == description &&
        o.id == id &&
        o.image == image &&
        o.ingredients == ingredients &&
        o.liked == liked &&
        o.popular == popular &&
        o.price == price &&
        o.title == title;
  }

  @override
  int get hashCode {
    return backgroundImage.hashCode ^
        caption.hashCode ^
        category.hashCode ^
        description.hashCode ^
        id.hashCode ^
        image.hashCode ^
        ingredients.hashCode ^
        liked.hashCode ^
        popular.hashCode ^
        price.hashCode ^
        title.hashCode;
  }
}
