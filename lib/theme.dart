import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart' show SizeExtension;

// Main Colors
const Color aquamarine = Color(0xFF00D4C5);
const Color purple = Color(0xFF5B00B3);
const Color salmon = Color(0xFFFF6C93);
const Color darkBlue = Color(0xFF033E77);
const Color darkGrey = Color(0xFF4E5F66);
const Color grey = Color(0xFF9A9A9A);
const Color lightGrey = Color(0xFFBBBBBB);
const Color lightestGrey = Color(0xFFF4F4F4);

// Gradient
const List<Color> buttonGradient = [Color(0xFF530098), Color(0xFF00D9C4)];

class DeliveryTheme {
  static get theme {
    final textTheme = TextTheme(
      bodyText1: TextStyle(
        fontSize: 11.5.w,
        color: darkBlue,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w300,
      ),
      bodyText2: TextStyle(
        color: grey,
        fontSize: 14.w,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w200,
      ),
      headline1: TextStyle(
        fontSize: 15.w,
        color: darkBlue,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
      ),
      headline2: TextStyle(
        fontSize: 16.w,
        color: Colors.white,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w500,
      ),
      headline3: TextStyle(
        fontSize: 20.w,
        color: purple,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
      ),
      caption: TextStyle(
        fontSize: 11.w,
        color: darkGrey,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w300,
      ),
    );
    return ThemeData(
      fontFamily: 'Poppins',
      primaryColor: purple,
      accentColor: aquamarine,
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      textTheme: textTheme,
      primaryTextTheme: textTheme,
      pageTransitionsTheme: PageTransitionsTheme(
        builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        },
      ),
    );
  }
}
