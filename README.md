[![Flutter](https://badgen.net/badge/Flutter/v2.13.0/0e83cd)](https://flutter.dev/docs/development/tools/sdk/releases)

# Delivery App

Delivery app

## Info ##

This project uses:

* [Flutter][1] as mobile development framework
* [Provider][2] as state manager

## Setup ##

First, you need to copy `.env.example` to a new file `.env` and replace all the needed variables.

Then, for install packages:

```
flutter pub get
```

To start the application in development mode (load all services locally with hot-reload & REPL):

```
flutter run
```

## Contribution guidelines ##

The rules to submit a contribution are:

* Write only on English
* Don't make push on master
* Do a rebase before merge request
* Request a review before merge
* Limit your text lines to 80 characters or fewer
* Add a break of line on any file
* Make atomic commits
* Follow the [git message][3] format using the regex:

```
((^[A-Z]{1})([a-z\ A-Z]+[a-z])(\n\n)((.)+([\n]{1,2})?)+)([\n\n]((Close:\ )|(See\ also:\ ) | (Resolves:\ ))\#[0-9]+)?
```

* Create a new branch before uploading any change, using the regex:

```
((feature)|((hot)?fix))\/([a-z]+(-?[a-z0-9]*)*)([a-z0-9])$
```

## Contact ##

[![Joel Romero](https://badgen.net/badge/Contact/Joel%20Romero/e24329?icon=gitlab)](https://gitlab.com/joelrg1288)

## TODO ##

- [ ] Test
- [ ] CI/CD

## Useful links

* [Design](https://dribbble.com/shots/14173151-Delivery-app)
* [Provider Docs](https://pub.dev/documentation/provider/latest/)

[1]: https://flutter.dev

[2]: https://pub.dev/packages/provider

[3]: https://thoughtbot.com/blog/better-commit-messages-with-a-gitmessage-template
